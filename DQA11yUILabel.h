//
//  DQA11yUILabel.h
//  TestAppDeque
//
//  Created by Chris McMeeking on 11/14/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DQA11yUILabelCol1 : UILabel

@end

@interface DQA11yUILabelCol2 : UILabel

@end
