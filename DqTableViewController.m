//
//  DqTableViewController.m
//  TestAppDeque
//
//  Created by Chris McMeeking on 11/5/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import "DqTableViewController.h"

#define KEY_SECTION_HEADING @"DQ_section_heading"
#define KEY_CHILDREN @"DQ_children"
#define KEY_NAME @"DQ_name"
#define KEY_NUMBER @"DQ_number"

@implementation DqTableViewController {
    NSMutableArray* _Sections;
    __weak IBOutlet UITableView *_TableView;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [_TableView setDataSource:self];
    
    NSLog(@"Initializing");
    _Sections = [[NSMutableArray alloc] init];
    NSMutableDictionary* section1 = [[NSMutableDictionary alloc] init];
    [section1 setObject:@"Co-Workers" forKey:KEY_SECTION_HEADING];
    [section1 setObject:[NSMutableArray array] forKey:KEY_CHILDREN];
    
    [[section1 objectForKey:KEY_CHILDREN] addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Chris McMeeking", KEY_NAME, @"734-645-5506", KEY_NUMBER, nil]];
    
    [[section1 objectForKey:KEY_CHILDREN] addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Dylan Barrell", KEY_NAME, @"777-666-5555", KEY_NUMBER, nil]];
    
    [_Sections addObject:section1];
    
    section1 = [[NSMutableDictionary alloc] init];
    [section1 setObject:@"Family and Friends" forKey:KEY_SECTION_HEADING];
    [section1 setObject:[NSMutableArray array] forKey:KEY_CHILDREN];
    
    [[section1 objectForKey:KEY_CHILDREN] addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Josh McMeeking", KEY_NAME, @"734-645-5506", KEY_NUMBER, nil]];
    
    [[section1 objectForKey:KEY_CHILDREN] addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Cheryl McMeeking", KEY_NAME, @"777-666-5555", KEY_NUMBER, nil]];
    
    [_Sections addObject:section1];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[_Sections objectAtIndex:section] objectForKey:KEY_CHILDREN] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = nil;
    
    if ([indexPath section] == 0) {
        identifier = @"CellIdentifer1";
    } else {
        identifier = @"CellIdentifer2";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary* person = [[[_Sections objectAtIndex:[indexPath section]] objectForKey:KEY_CHILDREN] objectAtIndex:[indexPath item]];
        
    [(UILabel*)[cell viewWithTag:1] setText:[person objectForKey:KEY_NAME]];
    
    [(UILabel*)[cell viewWithTag:2] setText:[person objectForKey:KEY_NUMBER]];
    
    [(UIButton*)[cell viewWithTag:3] addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchDown];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"Coujt: %lu", (unsigned long)_Sections.count);
    return [_Sections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[_Sections objectAtIndex:section] objectForKey:KEY_SECTION_HEADING];
}

- (void) buttonPress:(UIButton*)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You pushed a button"
                                                    message:@"The button had the following value: "
                                                   delegate:nil
                                          cancelButtonTitle:@"Go Away"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
