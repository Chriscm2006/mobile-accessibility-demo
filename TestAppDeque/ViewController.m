//
//  ViewController.m
//  TestAppDeque
//
//  Created by Chris McMeeking on 3/6/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController 
 
- (void)viewDidLoad
{
    [super viewDidLoad];
    [googleButton addTarget:self action:@selector(launchGoogleInSafari) forControlEvents:UIControlEventTouchDown];
    [googleViewAction addTarget:self action:@selector(launchGoogleInSafari)];
}


- (void)launchGoogleInSafari {
    NSURL * url = [NSURL URLWithString:@"http://www.google.com"];
    
    if (![[UIApplication sharedApplication] openURL:url]) {
        NSLog(@"Chris: Failed to open url: %@", [url description]);
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
