//
//  main.m
//  TestAppDeque
//
//  Created by Chris McMeeking on 3/6/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
