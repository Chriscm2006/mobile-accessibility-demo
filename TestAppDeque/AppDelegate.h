//
//  AppDelegate.h
//  TestAppDeque
//
//  Created by Chris McMeeking on 3/6/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
