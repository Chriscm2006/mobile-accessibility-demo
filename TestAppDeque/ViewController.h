//
//  ViewController.h
//  TestAppDeque
//
//  Created by Chris McMeeking on 3/6/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    IBOutlet UIButton *googleButton;
    IBOutlet UIButton *googleButton1;
    IBOutlet UITapGestureRecognizer *googleViewAction;
    IBOutlet UIView *googleView;
}

@end
