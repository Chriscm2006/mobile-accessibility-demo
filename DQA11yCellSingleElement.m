//
//  DQA11yCellSingleElement.m
//  TestAppDeque
//
//  Created by Chris McMeeking on 11/14/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import "DQA11yCellSingleElement.h"

@implementation DQA11yCellSingleElement

- (BOOL)isAccessibilityElement {
    return YES;
}
- (NSString*)accessibilityLabel {
    NSString* name = [(UILabel*)[self viewWithTag:1] text];
    NSString* number = [(UILabel*)[self viewWithTag:2] text];
    NSString* formatString = @"Name: %@ Phone Number: %@";
    return [NSString stringWithFormat:formatString,
            name,
            number];
}
- (NSString*)accessibilityHint {
    return @"Launches Alert";
}
- (UIAccessibilityTraits)accessibilityTraits {
    return UIAccessibilityTraitButton;
}

@end
