//
//  DQA11yUILabel.m
//  TestAppDeque
//
//  Created by Chris McMeeking on 11/14/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import "DQA11yUILabel.h"

@implementation DQA11yUILabelCol1

- (NSString*)accessibilityLabel {
    return [NSString stringWithFormat:@"Name: %@",[self text]];
}

- (BOOL)isAccessibilityElement {
    return YES;
}

@end

@implementation DQA11yUILabelCol2

- (NSString*)accessibilityLabel {
    return [NSString stringWithFormat:@"Number: %@",[self text]];
}

- (BOOL)isAccessibilityElement {
    return YES;
}

@end