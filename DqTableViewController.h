//
//  DqTableViewController.h
//  TestAppDeque
//
//  Created by Chris McMeeking on 11/5/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DqTableViewController : UIViewController <UITableViewDataSource>

@end
