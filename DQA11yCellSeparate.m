//
//  DQA11yCellSeparate.m
//  TestAppDeque
//
//  Created by Chris McMeeking on 11/14/13.
//  Copyright (c) 2013 Chris McMeeking. All rights reserved.
//

#import "DQA11yCellSeparate.h"

@implementation DQA11yCellSeparate

- (NSArray*)accessibilityElements {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    
    [result addObject:[self viewWithTag:1]];
    [result addObject:[self viewWithTag:2]];
    [result addObject:[self viewWithTag:3]];
    return result;
}

- (NSInteger)accessibilityElementCount {
    return [[self accessibilityElements] count];
}

- (id)accessibilityElementAtIndex:(NSInteger)index {
    return [[self accessibilityElements] objectAtIndex:index];
}

- (NSInteger)indexOfAccessibilityElement:(id)element {
    return [[self accessibilityElements] indexOfObject:element];
}

- (BOOL)isAccessibilityElement {
    return NO;
}

@end
